<?php namespace Pascalnegwer\Weeklymenus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePascalnegwerWeeklymenusDish extends Migration
{
    public function up()
    {
        Schema::create('pascalnegwer_weeklymenus_dish', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->double('price', 10, 0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pascalnegwer_weeklymenus_dish');
    }
}
