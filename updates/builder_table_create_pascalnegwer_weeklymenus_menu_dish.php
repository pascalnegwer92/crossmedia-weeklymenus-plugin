<?php namespace Pascalnegwer\Weeklymenus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePascalnegwerWeeklymenusMenuDish extends Migration
{
    public function up()
    {
        Schema::create('pascalnegwer_weeklymenus_menu_dish', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('menu_id');
            $table->integer('dish_id');
            $table->primary(['menu_id','dish_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pascalnegwer_weeklymenus_menu_dish');
    }
}
