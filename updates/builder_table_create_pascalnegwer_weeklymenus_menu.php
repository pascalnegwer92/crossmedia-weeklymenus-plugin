<?php namespace Pascalnegwer\Weeklymenus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePascalnegwerWeeklymenusMenu extends Migration
{
    public function up()
    {
        Schema::create('pascalnegwer_weeklymenus_menu', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pascalnegwer_weeklymenus_menu');
    }
}
