<?php namespace Pascalnegwer\Weeklymenus\Models;

use October\Rain\Database\Builder;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

/**
 * @field
 * @method static Builder where(string $key, mixed $operator, mixed $value = null)
 */
class Dish extends Model
{
    use Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pascalnegwer_weeklymenus_dish';
}
