<?php namespace Pascalnegwer\Weeklymenus;

use Illuminate\Console\Scheduling\Schedule;
use Pascalnegwer\Weeklymenus\Models\Menu;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function register()
    {
        $this->registerConsoleCommand('weeklymenus:export', 'PascalNegwer\WeeklyMenus\Console\Export');
        $this->registerConsoleCommand('weeklymenus:import', 'PascalNegwer\WeeklyMenus\Console\Import');
    }

    /**
     * @param string|Schedule $schedule
     */
    public function registerSchedule($schedule)
    {
        $schedule->call('weeklymenus:export')->everyMinute();
        $schedule->call('weeklymenus:import')->everyMinute();
    }
}
