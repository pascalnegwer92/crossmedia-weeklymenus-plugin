<?php namespace PascalNegwer\WeeklyMenus\Console;

use Illuminate\Console\Command;
use Pascalnegwer\Weeklymenus\Models\Dish;
use Pascalnegwer\Weeklymenus\Models\Menu;

class Import extends Command
{
    /**
     * @var string $name Der Name unter dem der Command in der Kommandozeile ausgeführt werden kann.
     * Bsp: php artisan weeklymenus:export
     */
    protected $name = 'weeklymenus:import';

    /**
     * Dies ist die Funktion, die ausgeführt wird sobald der Command ausgeführt wurde
     * @return void
     */
    public function handle()
    {
        $menu = new \SimpleXMLElement(file_get_contents(temp_path('import/1.xml')));
        $builder = Menu::where('id', $menu->attributes()->cardId);
        $builder->update([
                'title' => $menu->cardTitle,
                'description' => $menu->cardDescription
            ]);

        $originalMenu = $builder->firstOrFail();

        foreach ($originalMenu->images as $image) {
            $image->delete();
        }

        $originalMenu = $originalMenu->fresh();

        if ($handle = opendir($this->getImportFilePath('Bilder'))) {
            echo "Directory handle: $handle\n";
            echo "Entries:\n";

            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $originalMenu->images()->create(['data' => $this->getImportFilePath('Bilder/' . $entry)]);
                }
            }

            closedir($handle);
        }

        foreach ($menu->dish as $dish) {
            $dishCollection = Dish::where('id', $dish->attributes()->dishId);
            $dishCollection->update([
                'title' => $dish->dishTitle,
                'description' => $dish->dishDescription,
                'price' => floatval(str_replace(['.', ','], ['', '.'], $dish->dishPrice))
            ]);
        }
    }

    /**
     * @param string $fileName Der Name des Files für den der Pfad aus dem importiert wird erstellt werden soll
     * @return string Der Dateipfad einer Datei die importiert werden soll
     */
    private function getImportFilePath(string $fileName): string
    {
        $importFilePath = temp_path('import');
        if (!is_dir($importFilePath)) {
            mkdir($importFilePath);
        }
        return $importFilePath . '/' .  $fileName;
    }
}
