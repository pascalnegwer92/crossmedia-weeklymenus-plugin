<?php namespace PascalNegwer\WeeklyMenus\Console;

use Illuminate\Console\Command;
use Pascalnegwer\Weeklymenus\Models\Menu;

class Export extends Command
{
    /**
     * @var string $name Der Name unter dem der Command in der Kommandozeile ausgeführt werden kann.
     * Bsp: php artisan weeklymenus:export
     */
    protected $name = 'weeklymenus:export';

    /**
     * Dies ist die Funktion, die ausgeführt wird sobald der Command ausgeführt wurde
     * @return void
     */
    public function handle()
    {
        $menus = Menu::all();
        foreach ($menus as $menu) {
            $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8" ?><weeklyCard cardId="' . $menu->id . '"></weeklyCard>');
            $xml->addChild('cardTitle', $menu->title)->addAttribute('titleId', $menu->id);
            $xml->addChild('cardDescription', $menu->description)->addAttribute('descriptionId', $menu->id);
            foreach ($menu->dish as $dish) {
                $child = $xml->addChild('dish');
                $child->addAttribute('dishId', $dish->id);
                $child->addChild('dishTitle', $dish->title)->addAttribute('dishTitleId', $dish->id);
                $child->addChild('dishDescription', $dish->description)->addAttribute('dishDescriptionId', $dish->id);
                $child->addChild('dishPrice', number_format($dish->price, 2, ',', '.'))->addAttribute('dishPriceId', $dish->id);
            }
            foreach ($menu->images as $image) {
                $child = $xml->addChild('image');
                $child->addAttribute('imageId', $image->id);
                $child->addAttribute('href', 'file:///Bilder/' . $image->disk_name);
                copy($image->getLocalPath(), $this->getExportImagePath($image->disk_name));
            }

            $xml->asXML($this->getExportFilePath($menu->id . '.xml'));
        }
    }

    /**
     * @param string $fileName Der Name des Files für den der Exportpfad erstellt werden soll
     * @return string Der Dateipfad einer Datei im Export-Verzeichnis
     */
    private function getExportFilePath(string $fileName): string
    {
        $exportFilePath = temp_path('export');
        if (!is_dir($exportFilePath)) {
            mkdir($exportFilePath);
        }
        return $exportFilePath . '/' .  $fileName;
    }

    /**
     * @param string $imageName Der Dateiname des Bildes für das den Exportpfard erstellt werden soll
     * @return string Der Dateipfad eines Bildes im Export-Verzeichnis
     */
    private function getExportImagePath(string $imageName): string
    {
        $exportImagePath = $this->getExportFilePath('Bilder');
        if (!is_dir($exportImagePath)) {
            mkdir($exportImagePath);
        }
        return $exportImagePath . '/' . $imageName;
    }
}
